Website
=======
http://sourceforge.net/projects/luabind/

License
=======
Custom license (see the file source/LICENSE)

Version
=======
0.7.1

Source
======
luabind-0.7.1.tar.gz (sha256: 173491dd1d845a8d01bb9e92636659c23d239b396d63813fb6ff2bfa02a3561f)

Requires
========
* boost
* lua50
